[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Dependency
```bash
sudo apt install -y libqt5serialport5-dev
```

# Run example
For 1 data per line
```bash
rosrun serial_temperatures publishers _count:=1 _port:=/dev/ttyUSB0 _topic:=/ram/sensors/temperature_adafruit/
```

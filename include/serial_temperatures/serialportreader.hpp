#ifndef SERIAL_TEMPERATURES_SERIALPORTREADER_HPP
#define SERIAL_TEMPERATURES_SERIALPORTREADER_HPP

#include <memory>
#include <QByteArray>
#include <QCoreApplication>
#include <QDateTime>
#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <ros/publisher.h>
#include <ros/ros.h>
#include <std_msgs/Float32.h>

class SerialPortReader : public QObject
{
  Q_OBJECT

public:
  SerialPortReader(QSerialPort *serial_port, const unsigned number_of_data, QObject *parent = 0);
  ~SerialPortReader();

  std::shared_ptr<std::vector<ros::Publisher>> publishers_;
  std::shared_ptr<ros::NodeHandle> nh_;

Q_SIGNALS:
  void newLineFetched(QString line, QDateTime date_time);

private Q_SLOTS:
  void readData();
  void handleError(QSerialPort::SerialPortError error);
  void publishTemperatures(QString line, QDateTime date_time);

private:
  unsigned number_of_data_;
  QSerialPort *serial_port_;
  QByteArray data_read_;
};

#endif

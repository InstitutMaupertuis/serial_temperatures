#include <QtCore>
#include <ros/ros.h>
#include <serial_temperatures/serialportreader.hpp>

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "serial_temperatures");
  std::shared_ptr<ros::NodeHandle> nh(std::make_shared<ros::NodeHandle>("~"));

  int count(1);
  nh->getParam("count", count);
  if (count < 1)
  {
    ROS_ERROR_STREAM("count cannot be < 1");
    return 1;
  }
  ROS_INFO_STREAM("Count = " << count);

  std::string topic;
  nh->getParam("topic", topic);
  if (topic.empty())
  {
    ROS_ERROR_STREAM("topic param must be specified and not empty");
    return 1;
  }
  ROS_INFO_STREAM("Topic = " << topic);

  ros::AsyncSpinner s(2);
  s.start();
  std::shared_ptr<std::vector<ros::Publisher>> publishers;
  publishers = std::make_shared<std::vector<ros::Publisher>>();
  publishers->resize(count);
  for (std::size_t i(0); i < publishers->size(); ++i)
    publishers->at(i) = nh->advertise<std_msgs::Float32>(topic + std::to_string(i), 5);

  QCoreApplication app(argc, argv);
  QSerialPort *serial_port(new QSerialPort);
  SerialPortReader *serial_port_reader(new SerialPortReader(serial_port, count));
  serial_port_reader->publishers_ = publishers;
  serial_port_reader->nh_ = nh;

  std::string port("/dev/ttyACM0");
  nh->getParam("port", port);
  int baudrate(115200);
  nh->getParam("baudrate", baudrate);

  ROS_INFO_STREAM("Port = " << port);
  ROS_INFO_STREAM("Baudrate = " << baudrate);

  serial_port->setPortName(port.c_str());
  serial_port->setBaudRate(baudrate);
  if (!serial_port->open(QIODevice::ReadOnly))
  {
    ROS_ERROR_STREAM("Could not open port " << serial_port->portName().toStdString());
    return 1;
  }

  ROS_INFO_STREAM("Start publishing");
  return app.exec();
}

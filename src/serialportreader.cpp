#include <serial_temperatures/serialportreader.hpp>

SerialPortReader::SerialPortReader(QSerialPort *serial_port,
                                   const unsigned number_of_data,
                                   QObject *parent) :
  QObject(parent),
  number_of_data_(number_of_data),
  serial_port_(serial_port)
{
  connect(serial_port, &QSerialPort::readyRead, this, &SerialPortReader::readData);
  connect(serial_port, qOverload<QSerialPort::SerialPortError>(&QSerialPort::error), this, &SerialPortReader::handleError);
  connect(this, &SerialPortReader::newLineFetched, this, &SerialPortReader::publishTemperatures);
}

SerialPortReader::~SerialPortReader()
{
}

void SerialPortReader::readData()
{
  data_read_.append(serial_port_->readAll());
  if (!data_read_.contains('\n'))
    return; // Wait for a complete line

  // Keep only last message if there are multiple ones
  const int last_lr(data_read_.lastIndexOf('\n'));

  // Remove extra characters after last \n
  if (last_lr != data_read_.size() - 1)
    data_read_.truncate(last_lr + 1);

  // While first \n is not last
  int index(data_read_.indexOf('\n'));
  while (index != data_read_.size() - 1)
  {
    // Remove characters on the left
    data_read_ = data_read_.right(data_read_.size() - index - 1);

    // Search for first \n, don't stop until it's the last character
    index = data_read_.indexOf('\n');
  }

  QDateTime now(QDateTime::currentDateTime()); // Not good because it's the stamp of the last byte message!
  Q_EMIT newLineFetched(data_read_, now);
  data_read_.clear();
}

void SerialPortReader::handleError(QSerialPort::SerialPortError errror)
{
  if (errror == QSerialPort::ReadError)
  {
    ROS_ERROR_STREAM("An I/O error occurred while reading the data from port " << serial_port_->portName().toStdString()
                     << ", error:" << serial_port_->errorString().toStdString());
  }
}

void SerialPortReader::publishTemperatures(QString line, QDateTime)
{
  if (!publishers_)
    return;

  QRegExp rx("[;]");
  QStringList list = line.split(rx, QString::SkipEmptyParts);
  if (list.size() != (int) number_of_data_)
  {
    ROS_WARN_STREAM("Wrong message received. Expected " << number_of_data_ <<", got " << list.size());
    return;
  }

  std_msgs::Float32 msg;
  for (unsigned i(0); i < number_of_data_; ++i)
  {
    bool ok;
    double value(list.at(i).toDouble(&ok)); // Check that conversion is successful
    if (!ok)
    {
      ROS_ERROR_STREAM("Could not convert " << list.at(i).toStdString());
      break;
    }

    msg.data = value;
    publishers_->at(i).publish(msg);
  }
}
